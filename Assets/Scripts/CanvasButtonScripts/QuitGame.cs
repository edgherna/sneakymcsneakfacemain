﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    public void QuitButton_Click()
    {
        //This debug log is to make sure to let you know in unity that the game will exit since this will happen when the application is done.
        Debug.Log("Game Ended! The Application closes down, ending the game.");

        
        Application.Quit();
        
    }

    //The below code is to double check if the Quit application function is working.
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Debug.Log("Game Ended! The Application closes down, ending the game.");

            /*
            Application.Quit();
            */
        }
    }

}
