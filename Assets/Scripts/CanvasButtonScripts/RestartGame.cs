﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour
{

    public void RestartButton_Click()
    {
        //When the game restarts we make sure that the player starts at its origin starting position.
        GameManager.instance.playerOrigin.transform.position = new Vector3(GameManager.instance.origin.x,
                                                                           GameManager.instance.origin.y,
                                                                           GameManager.instance.origin.z);
        GameManager.instance.SetGameState((int)GameManager.GameState.Start);
    }

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
