﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BeginGame : MonoBehaviour
{
    public void BeginButton_Click()
    {
        GameManager.instance.SetGameState((int)GameManager.GameState.Level);
    }

	// Use this for initialization
	void Start()
    {

	}
	
	// Update is called once per frame
	void Update() {
		
	}
}
