﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreenGame : MonoBehaviour
{
    //This is where the title screen begins.
    public void TitleScreenButton_Click()
    {
        //The below state is to make sure that the player is starting on its original position
        GameManager.instance.playerOrigin.transform.position = new Vector3(GameManager.instance.origin.x,
                                                                           GameManager.instance.origin.y,
                                                                           GameManager.instance.origin.z);
        GameManager.instance.SetGameState((int)GameManager.GameState.Start);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
