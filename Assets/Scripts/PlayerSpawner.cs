﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    public GameObject playerPrefab;
    GameObject playerInstance;

    public int numLives = 1;

    public float respawnTimer;

	// Use this for initialization
	void Start()
    {
        SpawnPlayer();
	}

    void SpawnPlayer()
    {
        numLives--;
        playerInstance = (GameObject)Instantiate(playerPrefab, transform.position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update()
    {
		if (playerInstance == null && numLives > 0)
        {
            respawnTimer -= Time.deltaTime;

            if (respawnTimer <= 0)
            {
                SpawnPlayer();
            }
        }
	}
}
