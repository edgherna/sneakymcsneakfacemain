﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //The following below is a list tracker for the canvas
    public GameObject mStartCanvas;
    public GameObject mLevelCanvas;
    public GameObject mMenuCanvas;
    public GameObject mWinCanvas;
    public GameObject mGameOverCanvas;
    public GameObject playerOrigin;
    public Vector3 origin;

    bool menuState = false;                 //False is off, true is on.

    public static GameManager instance;

    public enum GameState
    {
        Start, //0
        Level, //1
        Menu, //2
        Win, //3
        GameOver //4
    }

    public GameState gameState;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(instance);
        }
    }

	// Use this for initialization
	void Start ()
    {
        origin = new Vector3(playerOrigin.transform.position.x, playerOrigin.transform.position.y, playerOrigin.transform.position.z);
        SetGameState((int)GameState.Start);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void ActivateScreens()
    {
        switch(gameState)
        {
            case GameState.Start:
                mStartCanvas.SetActive(true);
                mLevelCanvas.SetActive(false);
                mMenuCanvas.SetActive(false);
                mWinCanvas.SetActive(false);
                mGameOverCanvas.SetActive(false);
                break;

            case GameState.Level:
                mStartCanvas.SetActive(false);
                mLevelCanvas.SetActive(true);
                mMenuCanvas.SetActive(false);
                mWinCanvas.SetActive(false);
                mGameOverCanvas.SetActive(false);
                break;

            case GameState.Menu:
                mStartCanvas.SetActive(false);
                mLevelCanvas.SetActive(true);                  //Because this is a menu, the level canvas will also be on.

                if (menuState == true)
                {
                    mMenuCanvas.SetActive(false);
                    menuState = false;
                }
                else
                {
                    mMenuCanvas.SetActive(true);
                    menuState = true;
                }

                mWinCanvas.SetActive(false);
                mGameOverCanvas.SetActive(false);
                break;

            case GameState.Win:
                mStartCanvas.SetActive(false);
                mLevelCanvas.SetActive(false);
                mMenuCanvas.SetActive(false);
                mWinCanvas.SetActive(true);
                mGameOverCanvas.SetActive(false);
                break;

            case GameState.GameOver:
                mStartCanvas.SetActive(false);
                mLevelCanvas.SetActive(false);
                mMenuCanvas.SetActive(false);
                mWinCanvas.SetActive(false);
                mGameOverCanvas.SetActive(true);
                break;
        }
    }

    public void SetGameState(int state)
    {
        gameState = (GameState)state;
        ActivateScreens();
    }
}
