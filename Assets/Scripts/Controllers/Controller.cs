﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{
    //This is just an abstract class for the pawn.
    public Pawn pawn;

	public virtual void Start ()
    {
        pawn = GetComponent<Pawn>();
	}
}