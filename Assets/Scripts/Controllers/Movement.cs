﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float maxSpeed = 2f;                 //The speed of its movement.
    public float rotSpeed = 180f;               //The speed of its rotation.
    public Vector3 origin;                      //This Vector3 is to record the player's starting position.

	// Use this for initialization
	void Start ()
    {
        origin = new Vector3(transform.position.x, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update ()
    {
        gameMenu();
        //The following below is to control the movement for the x-axis.
        Quaternion rot = transform.rotation;
        float z = rot.eulerAngles.z;
        z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
        rot = Quaternion.Euler(0, 0, z);
        transform.rotation = rot;

        //The following below is to control the movement for the y-axis.
        Vector3 pos = transform.position;
        Vector3 velocity = new Vector3(0, Input.GetAxis("Vertical") * maxSpeed * Time.deltaTime, 0);
        pos += rot * velocity;
        transform.position = pos;
	}

    //This is the collision
    void OnTriggerEnter2D(Collider2D other)
    {
        //If the player collides with the enemy, it is over.
        if (other.gameObject.tag == "Enemy")
        {
            transform.position = new Vector3(origin.x, origin.y, origin.z);
            GameManager.instance.SetGameState((int)GameManager.GameState.GameOver);
        }

        //If the player collides with the goal, a winning message is brought up.
        if (other.gameObject.tag == "Finish")
        {
            //transform.position = new Vector3(origin.x, origin.y, origin.z);
            GameManager.instance.playerOrigin.transform.position = new Vector3(GameManager.instance.origin.x,
                                                                               GameManager.instance.origin.y,
                                                                               GameManager.instance.origin.z);
            GameManager.instance.SetGameState((int)GameManager.GameState.Win);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Background")
        {
            //transform.position = new Vector3(origin.x, origin.y, origin.z);
            GameManager.instance.playerOrigin.transform.position = new Vector3(GameManager.instance.origin.x,
                                                                               GameManager.instance.origin.y,
                                                                               GameManager.instance.origin.z);
            GameManager.instance.SetGameState((int)GameManager.GameState.GameOver);
        }
    }

    //This module will be checking constantly until the player hits the menu button
    void gameMenu()
    {
        if (Input.GetKey(KeyCode.T))
        {
            GameManager.instance.SetGameState((int)GameManager.GameState.Menu);
        }
    }
}
