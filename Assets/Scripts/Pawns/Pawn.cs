﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour
{
    //The following code below is for the pawns to inherit from the parent pawn class.
    public Transform tf;
    public Vector3 homePosition;

    public float speed = 10.0f;
    public float rotationSpeed = 180.0f;

	// Use this for initialization
	public virtual void Start ()
    {
        tf = GetComponent<Transform>();

        homePosition = tf.position;
	}
	
	// Update is called once per frame
	public virtual void Update ()
    {
		
	}

    public virtual void MoveForward()
    {

    }

    public virtual void MoveBackward()
    {

    }

    public virtual void RotateLeft()
    {

    }

    public virtual void RotateRight()
    {

    }
}
